#!/usr/bin/env ruby

if ARGV.any?
  require './classes'
  require ARGV.at(0)

  model = Model.new($grid)
  
  solver = Solver.new(model, $discFac)
  solver.value_iteration(1e-5, 100)
  
  puts model.pretty_policy(solver.policy)
  
  puts model.pretty_value(solver.value)
  
  File.open(ARGV.at(1), 'w') {|f| model.pretty_value(solver.value).each {|l| f.write("%s\n" % l)}} unless (ARGV.at(1).nil?)
else
  raise "Config file name without extension must be passed as an argument"
end