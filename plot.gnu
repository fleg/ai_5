set terminal pngcairo  transparent enhanced font "arial,10" fontscale 1.0 size 500, 350 
unset key
set view map
set xtics border in scale 0,0 mirror norotate  offset character 0, 0, 0 autojustify
set ytics border in scale 0,0 mirror norotate  offset character 0, 0, 0 autojustify
set ztics border in scale 0,0 nomirror norotate  offset character 0, 0, 0 autojustify
set nocbtics
set rtics axis in scale 0,0 nomirror norotate  offset character 0, 0, 0 autojustify
set xrange [ -0.500000 : 3.50000 ] noreverse nowriteback
set yrange [ -0.500000 : 3.50000 ] noreverse nowriteback
set cblabel "Score" 
set cbrange [ 70.00000 : 100.00000 ] noreverse nowriteback
set palette rgbformulae -7, 2, -7
splot 'data.dat' matrix with image