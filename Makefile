heatmap.png: data.dat plot.gnu
	gnuplot plot.gnu > heatmap.png

data.dat: project.rb config.rb classes.rb
	ruby project.rb ./config ./data.dat

clean:
	rm heatmap.png
	rm data.dat