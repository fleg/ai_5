#!/usr/bin/env ruby

require 'narray'

if __FILE__ == $0
  raise 'This file should not be run directly - please run project.rb'
end

class Solver
  def initialize model, discount, policy=nil, value=Hash.new(0)
    @model = model
    @discount = discount
    model_states = model.states
    state_to_num = Hash[model_states.zip((0...model_states.size).to_a)]
    @array_model = model_states.map {|state|
      model.actions(state).map {|action|
        model.next_states(state, action).map {|next_state|
          pr = model.transition_probability(state, action, next_state)
          [state_to_num[next_state], pr, 
            model.reward(state, action, next_state)] if pr > 0
        }.compact
      }
    }

    @array_value = model_states.map {|state| value[state]}
    if policy
      action_to_num = model_states.map{|state|
        actions = model.actions(state)
        Hash[actions.zip((0...actions.size).to_a)]
      }
      @array_policy = action_to_num.zip(model_states).
                            map {|a_to_n, state| a_to_n[policy[state]]}
    else
      @array_policy = [0]*model_states.size
    end

    raise 'values missing' if
      @array_value.any? {|v| v.nil?}
    raise 'policy actions missing' if
      @array_policy.any? {|a| a.nil?}

    @policy_A = nil
  end

  attr_reader :model

  def value
    Hash[model.states.zip(@array_value)]
  end

  def state_action_value
    q = {}
    states = model.states
    @array_model.each_with_index do |actions, state_n|
      state = states[state_n]
      state_actions = model.actions(state)
      actions.each_with_index do |next_state_ns, action_n|
        q_sa = next_state_ns.map {|next_state_n, pr, r|
          pr * (r + @discount * @array_value[next_state_n])}.inject(:+)
        q[[state, state_actions[action_n]]] = q_sa
      end
    end
    q
  end

  def policy
    Hash[model.states.zip(@array_policy).map{|state, action_n|
      [state, model.actions(state)[action_n]]}]
  end

  def evaluate_policy
    delta = 0.0
    @array_model.each_with_index do |actions, state_n|
      next_state_ns = actions[@array_policy[state_n]]
      new_value = backup(next_state_ns)
      delta = [delta, (@array_value[state_n] - new_value).abs].max
      @array_value[state_n] = new_value
    end
    delta
  end

  def evaluate_policy_exact
    if @policy_A
      # update only those rows for which the policy has changed
      @policy_A_action.zip(@array_policy).
        each_with_index do |(old_action_n, new_action_n), state_n|
        next if old_action_n == new_action_n
        update_policy_Ab state_n, new_action_n
      end
    else
      # initialise the A and the b for Ax = b
      num_states = @array_model.size
      @policy_A = NMatrix.float(num_states, num_states)
      @policy_A_action = [-1]*num_states
      @policy_b = NVector.float(num_states)

      @array_policy.each_with_index do |action_n, state_n|
        update_policy_Ab state_n, action_n
      end
    end

    value = @policy_b / @policy_A # solve linear system
    @array_value = value.to_a
    nil
  end

  def improve_policy
    stable = true
    @array_model.each_with_index do |actions, state_n|
      a_max = nil
      v_max = -Float::MAX
      actions.each_with_index do |next_state_ns, action_n|
        v = backup(next_state_ns)
        if v > v_max
          a_max = action_n
          v_max = v
        end
      end
      raise "no feasible actions in state #{state_n}" unless a_max
      stable = false if @array_policy[state_n] != a_max
      @array_policy[state_n] = a_max
    end
    stable
  end

  def value_iteration_single
    delta = 0.0
    @array_model.each_with_index do |actions, state_n|
      a_max = nil
      v_max = -Float::MAX
      actions.each_with_index do |next_state_ns, action_n|
        v = backup(next_state_ns)
        if v > v_max
          a_max = action_n
          v_max = v
        end
      end
      delta = [delta, (@array_value[state_n] - v_max).abs].max
      @array_value[state_n] = v_max
      @array_policy[state_n] = a_max
    end
    delta
  end

  def value_iteration tolerance, max_iters=nil
    delta = Float::MAX
    num_iters = 0
    loop do
      delta = value_iteration_single
      num_iters += 1

      break if delta < tolerance
      break if max_iters && num_iters >= max_iters
      yield num_iters, delta if block_given?
    end
    delta < tolerance
  end

  def policy_iteration value_tolerance, max_value_iters=nil,
    max_policy_iters=nil

    stable = false
    num_policy_iters = 0
    loop do
      # policy evaluation
      num_value_iters = 0
      loop do
        value_delta = evaluate_policy
        num_value_iters += 1

        break if value_delta < value_tolerance
        break if max_value_iters && num_value_iters >= max_value_iters
        yield num_policy_iters, num_value_iters, value_delta if block_given?
      end

      # policy improvement
      stable = improve_policy
      num_policy_iters += 1
      break if stable
      break if max_policy_iters && num_policy_iters >= max_policy_iters
    end
    stable
  end

  def policy_iteration_exact max_iters=nil
    stable = false
    num_iters = 0
    loop do
      evaluate_policy_exact
      stable = improve_policy
      num_iters += 1
      break if stable
      break if max_iters && num_iters >= max_iters
      yield num_iters if block_given?
    end
    stable
  end

  private

  def backup next_state_ns
    next_state_ns.map {|next_state_n, probability, reward|
      probability*(reward + @discount*@array_value[next_state_n])
    }.inject(:+)
  end

  def update_policy_Ab state_n, action_n
    # clear out the old values for state_n's row
    @policy_A[true, state_n] = 0.0

    # set new values according to state_n's successors under the current policy
    b_n = 0
    next_state_ns = @array_model[state_n][action_n]
    next_state_ns.each do |next_state_n, probability, reward|
      @policy_A[next_state_n, state_n] = -@discount*probability
      b_n += probability*reward
    end
    @policy_A[state_n, state_n] += 1
    @policy_A_action[state_n] = action_n
    @policy_b[state_n] = b_n
  end
end

class Model
  def initialize grid
    @grid = []
    @terminii = []
    grid.each_with_index do |row,rindex|
      curRow = []
      row.each_with_index do |element,eindex|
        result = case element
        when :normal   then $normVal
        when :special  then $specVal
        when :terminal
          @terminii.push([rindex,eindex])
          $termVal
        else nil  # by default it's a wall
        end
        curRow.push(result)
      end
      @grid.push(curRow)
    end    
  end

  attr_reader :grid, :terminii
  
  @@up = 'UP'
  @@down = 'DW'
  @@left = 'LF'
  @@right = 'RG'

  def states
    is, js = (0...grid.size).to_a, (0...grid.first.size).to_a
    is.product(js).select {|i, j| grid[i][j]} + [:stop]
  end

  MOVES = {
    @@up => [-1,  0], 
    @@right => [ 0,  1], 
    @@down => [ 1,  0], 
    @@left => [ 0, -1]} 

  def actions state
    if state == :stop || terminii.member?(state)
      [:stop]
    else
      MOVES.keys
    end
  end
  
  def next_states state, action
    states
  end

  def transition_probability state, action, next_state
    if state == :stop || terminii.member?(state)
      (action == :stop && next_state == :stop) ? 1 : 0
    else
      move = case action
             when @@up then [[@@up, $inInten], [@@left, $inOther], [@@right, $inOther]]
             when @@right then [[@@right, $inInten], [@@up, $inOther], [@@down, $inOther]]
             when @@down then [[@@down, $inInten], [@@left, $inOther], [@@right, $inOther]]
             when @@left then [[@@left, $inInten], [@@up, $inOther], [@@down, $inOther]]
             end
      move.map {|m, pr|
        m_state = [state[0] + MOVES[m][0], state[1] + MOVES[m][1]]
        m_state = state unless states.member?(m_state) # stay in bounds
        pr if m_state == next_state
      }.compact.inject(:+) || 0
    end
  end

  def reward state, action, next_state
    if state == :stop
      return 0
    else
      retval = grid[state[0]][state[1]]
      if retval == :surprise
        return 0-rand
      else
        return retval
      end
    end
  end
  
  def transition_probability_sums
    prs = []
    states.each do |state|
      actions(state).each do |action|
        pr = next_states(state, action).map{|next_state|
          transition_probability(state, action, next_state)}.inject(:+)
        prs << [[state, action], pr]
      end
    end
    Hash[prs]
  end

  def hash_to_grid hash
    0.upto(grid.size-1).map{|i| 0.upto(grid[i].size-1).map{|j| hash[[i,j]]}}
  end

  def pretty_value value
    hash_to_grid(Hash[value.map {|s, v| [s, "%+.3f" % v]}]).map{|row|
      row.map{|cell| (cell.nil? ? 0 : cell) || '      '}.join(' ')}
  end

  def pretty_policy policy
    hash_to_grid(policy).map{|row| row.map{|cell|
      (cell.nil? || cell == :stop) ? ' ' : cell}.join(' ')}
  end
  
  def terminal_states
    all_states = Set[]
    out_states = Set[]
    states.each do |state|
      all_states << state
      any_out_transitions = false
      actions(state).each do |action|
        ns = next_states(state, action)
        all_states.merge ns
        any_out_transitions ||= !ns.empty?
      end
      out_states << state if any_out_transitions 
    end
    all_states - out_states
  end
end