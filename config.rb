#!/usr/bin/env ruby

# Weights
$termVal = 100   # Terminal state
$specVal = -20   #  Special state
$normVal =  -1   #   Normal state

# Discounting Factor
$discFac = 0.99

# Probabilities
$inInten = 0.8   # Probability of landing in intended state
$inOther = 0.1   # Probability of landing in one of two other states

$grid= [[ :normal, :normal,  :normal,  :normal],
        [ :normal, :normal,  :normal,  :normal],
        [ :normal, :normal, :special,  :normal],
        [ :normal, :normal,      nil,:terminal]]